#! /bin/bash

# Jeżeli katalog z kodem wynikowym już istnieje.
if [ -d build ]
then
	# Odszukaj czas ostatniej modyfikacji kodu źródłowego i wynikowego.
	mtime_src="$(find src   -printf %Ts\\n | cat <(echo 0) - | sort -n | tail -n 1)";
	mtime_bui="$(find build -printf %Ts\\n | cat <(echo 0) - | sort -n | tail -n 1)";

	# Jeżeli kod wynikowy jest nowszy od kodu źródłowego, to zakończ skrypt.
	if [ $mtime_src -lt $mtime_bui ]
	then
		echo 'Build cancelled!'
		exit 1
	fi

	# Usuń katalog z kodem wynikowym.
	rm -Rf build
fi

# Komunikat o trwaniu budowania.
echo 'Building...'

# Utwórz katalog do przechowywania kodu wynikowego.
mkdir build

# Skopiuj do katalogu z kodem wynikowym potrzebne katalogi z kodu źródłowego.
cp -r src/css  build
cp -r src/js   build
#cp -r src/font build
cp -r src/img  build

# Połącz w kolejności alfabetycznej wszystkie pliki *.html znalezione w kodzie
# źródłowym i zapisz całość do pliku index.html w kodzie wynikowym.
find src -type f -name "*.html" | sort |\
xargs -d '\n' cat > build/index.html

# Utwórz obraz dockerowy
sudo docker build -t homepage .
