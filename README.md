# Home Page #

Strona domowa w formie jednostronicowego przewijanego portfolio.

## Budowanie ##

Budowanie strony odbywa się przy pomocy skryptu `build.bash`.
Strona po zbudowaniu zapisywana jest do katalogu `build`.

W celu jednokrotnego przebudowania strony, wykonaj następujące czynności:
* Przejdź do katalogu z repozytorium `cd homepage`.
* Uruchom polecenie `bash build.bash`.

Projekt przebuduje się tylko wtedy, kiedy modyfikacja plików z kodem źródłowym
nastąpiła po ostatnim przebudowaniu strony. W celu ręcznego wymuszenia
przebudowania strony, należy usunąć katalog `build` poleceniem `rm -Rf build`.

W celu uruchomienia ciągłego automatycznego przebudowania strony,
zamiast komendy `bash build.bash` wprowadź komendę `watch bash build.bash`.

## Pomysły ##

* <http://www.thepetedesign.com/demos/onepage_scroll_demo.html>

## Loga i ikony ##

| Nazwa         | Link                                                               |
|---------------|--------------------------------------------------------------------|
| HTML5         | <https://wikimediafoundation.org/wiki/File:HTML5_Badge.svg>        |
| CSS3          | <https://www.vinhlee.com/image/home/css-mobile.svg>                |
| JavaScript    | <https://seeklogo.com/vector-logo/330541/javascript>               |
| Bootstrap     | <https://v4-alpha.getbootstrap.com/about/brand/>                   |
| jQuery        | <https://dev.w3.org/SVG/tools/svgweb/samples/svg-files/jquery.svg> |
| Java          | <https://seeklogo.com/vector-logo/273556/java>                     |
| Spring        | <https://freebiesupply.com/logos/spring-logo-2/>                   |
| Hibernate     | <https://seeklogo.com/vector-logo/273522/hibernate>                |
| MySQL         | <https://worldvectorlogo.com/logo/mysql-6>                         |
| Git           | <https://git-scm.com/downloads/logos>                              |
| GitLab        | <https://commons.wikimedia.org/wiki/File:GitLab_Logo.svg>          |
| Atom          | <https://commons.wikimedia.org/wiki/File:Icon_Atom.svg>            |
| IntelliJ      | <https://wikimediafoundation.org/wiki/File:IntelliJ_IDEA_Logo.svg> |
| GoldenLine    | <https://www.goldenline.pl/images/4e39dee-923c2c7.svg>             |
| Facebook      | <https://commons.wikimedia.org/wiki/File:F_icon.svg>               |
| GitHub        | <https://commons.wikimedia.org/wiki/File:Octicons-mark-github.svg> |
| LinkedIn      | <https://commons.wikimedia.org/wiki/File:Linkedin.svg>             |
| StackOverflow | <https://stackoverflow.com/company/logos>                          |

* https://codepen.io/Redvanisation/pen/GywXzV

```bash
# Cropping SVG files in command line with Inkscape.
# https://shkspr.mobi/blog/2013/03/inkscape-cropping-svg-files-on-the-command-line/
ls -1 | grep '\.svg' | xargs -d '\n' -I % inkscape --verb=FitCanvasToDrawing --verb=FileSave --verb=FileQuit %

# Preview all SVG icons in html file.
ls -1 | grep '\.svg$' | sed 's/^/<img width=100 height=100 border=1 src="/' | sed 's/$/">/' > index.html
```
## tła ##

https://www.pexels.com/photo/close-up-photo-of-keyboard-220357/
https://www.pexels.com/photo/apple-devices-computer-contemporary-dark-234527/
https://www.pexels.com/photo/computer-desk-electronics-keyboard-238118/
https://www.pexels.com/photo/black-laptop-computer-keyboard-163130/
https://www.pexels.com/photo/antique-background-blur-book-268438/
https://www.pexels.com/photo/brown-paper-envelope-on-table-211290/
https://www.pexels.com/photo/photography-of-person-using-macbook-1251834/
https://www.pexels.com/photo/group-hand-fist-bump-1068523/
https://www.pexels.com/photo/antique-blank-camera-classic-269810/






## Linki ##

* <http://fontello.com/>
