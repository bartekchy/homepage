FROM nginx
EXPOSE 80/tcp
EXPOSE 443/tcp
COPY ./build /usr/share/nginx/html
